# Hackspace

My docker image for ~~hacking~~ penetration testing!

## Use this image

```bash
docker run -it --rm --log-driver none wcyat/hackspace
```
