FROM blackarchlinux/blackarch

WORKDIR /

RUN pacman-key --refresh-keys && pacman -Sy --noconfirm archlinux-keyring
RUN pacman -Syu --noconfirm sudo

RUN useradd -m user
RUN echo '%wheel ALL=(ALL:ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN usermod -aG wheel user

USER user

WORKDIR /home/user

RUN sudo pacman -Syu --noconfirm zsh fakeroot git binutils debugedit

RUN git clone https://aur.archlinux.org/yay-bin.git
RUN cd yay-bin && makepkg -sir --noconfirm
RUN rm -rf yay-bin

RUN yay -Syu --answerclean N --answerdiff N --answeredit N --answerupgrade y --removemake --noconfirm ripgrep secure-delete hydra nmap tor torsocks zsh-fast-syntax-highlighting sqlmap nosqli mysql postgresql mongosh-bin gobuster john openssh inetutils smbclient rockyou seclists evil-winrm responder links curl wget nano nano-syntax-highlighting whois ddosify bombardier mariadb mongosh-bin cupp python-pip gcc make pyenv patch proxybroker redis net-tools metasploit && pyenv install 3.7.17 && yay -Rs --noconfirm gcc make && yay -Scc --noconfirm
RUN yay -Scc --noconfirm

RUN /home/user/.pyenv/versions/3.7.17/bin/python -m pip install proxybroker
RUN cp -r /usr/share/cupp .

RUN git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh

COPY ./zshrc ./.zshrc

CMD ["/usr/bin/zsh"]

